// Claire Pickhardt
// Professor Bonham-Carter
// Lab 3
// 13 February 2017
// Honor Code: The work that I am submitting is the result of my own thinking and efforts.

#include<stdio.h>
#include<string.h>
#include<pthread.h>
#include<stdlib.h>
#include<unistd.h>
#define NUM_THREADS     2
// compile on Linux with: g++ -pthread threadMaker.c

void *PrintNum(void *threadid)
{
   long threadz;
   threadz = (long)threadid;
   pthread_exit(NULL);
}

//ProcA
  void *ProcA(){
    pthread_t threads[NUM_THREADS];
    int rc;
    long t;
    printf("Running ProcA:\n" );
    for(t=0; t<NUM_THREADS; t++){
       printf("   creating thread %ld\n", t);
       rc = pthread_create(&threads[t], NULL, PrintNum, (void *)t);
       if (rc){
          printf("ERROR; return code from pthread_create() is %d\n", rc);
          //exit(-1); //This line works on some systems when return doesn't work
          return 0;
       }
  }
}
//ProcB
void *ProcB(){
  pthread_t threads[NUM_THREADS];
  int rc;
  long t;
  printf("Running ProcB:\n");
  for(t=0; t<NUM_THREADS; t++){
     printf("   creating thread %ld\n", t);
     rc = pthread_create(&threads[t], NULL, PrintNum, (void *)t);
     if (rc){
        printf("ERROR; return code from pthread_create() is %d\n", rc);
        //exit(-1); //This line works on some systems when return doesn't work
        return 0;
     }
}
}
//ProcC
void *ProcC(){
  pthread_t threads[NUM_THREADS];
  int rc;
  long t;
  printf("Running ProcC:\n");
  for(t=0; t<NUM_THREADS; t++){
     printf("   creating thread %ld\n", t);
     rc = pthread_create(&threads[t], NULL, PrintNum, (void *)t);
     if (rc){
        printf("ERROR; return code from pthread_create() is %d\n", rc);
        //exit(-1); //This line works on some systems when return doesn't work
        return 0;
     }
}
}
//main
int main(){
  ProcA();
  ProcB();
  ProcC();
}
